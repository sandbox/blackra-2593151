<?php
/**
 * @file
 * Callback functions for regex_rule
 */

/**
 * Match a regex against some text, returning the first match and
 * any sub-patterns found
 *
 * @param $source_text string
 *   The text to perform the match on.
 * @param $regex string
 *   The regex match to perform
 * @param $settings
 * @param $state
 * @param $element
 * @return array
 *   Two variables as key-value pairs:
 *     is_match boolean
 *       TRUE if pattern $regex was found within the $source_text
 *     matches array
 *       An array of string matches. matches[0] corresponds to the whole
 *       expression and matches[1]..matches[n] are sub-pattern matches.
 */
function regex_match($source_text, $regex, $settings, $state, $element) {
  $matches = array();
  rules_log('regex_match called with source = "%source", regex = "%regex"',
    array(
      '%source' => $source_text,
      '%regex' => $regex,
    ));
  $match_flag = preg_match($regex, $source_text, $matches);
  rules_log('regex_match %did find match',
    array(
      '%did' => ($match_flag == 1) ? t('did') : t('did not'),
    ));

  $result = array(
    'is_match' => ($match_flag == 1),
    'matches' => $matches,
  );
  return $result;
}

/**
 * Match a regex against a text field and replace each match with a
 * replacement pattern. The caller is expected to modify the original
 * text field by replacing it with the 'text_field' member of the returned
 * array.
 *
 * @param $text_field
 *   The text field to perform a find and replace on
 * @param $regex
 *   The regular expression to find
 * @param $replacement_pattern
 *   The pattern for matches to be replaced with
 * @param $max_replacements
 *   Limit replacements to the first $max_replacements matches.
 *   -1 = replace all matches.
 * @param $settings
 * @param $state
 * @param $element
 * @return array
 *   The new value of the text field
 * @throws \RulesEvaluationException
 *   An error occurred, such as the regular expression being invalid.
 */
function regex_replace($text_field, $regex, $replacement_pattern,
                       $max_replacements, $settings, $state, $element) {
  rules_log('regex_replace called with text field = "%text_field", ' .
      'regex = "%regex", replace = "%replace", max replacments = "%max"',
    array(
      '%text_field' => $text_field,
      '%regex' => $regex,
      '%replace' => $replacement_pattern,
      '%max' => $max_replacements,
    ));
  $replace_result = preg_replace($regex, $replacement_pattern, $text_field, $max_replacements);

  if (is_null($replace_result)) {
    throw new RulesEvaluationException('Error %error in matching %source with %regex',
      array(
        '%error' => preg_last_error(),
        '%source' => $text_field,
        '%regex' => $regex,
      ),
      $element);
  }

  $result = array(
    'text_field' => $replace_result,
  );
  return $result;
}

/**
 * Match a regex against some text and return the result of replacing each
 * match with a replacement pattern. Does not modify the original text field.
 *
 * @param $source_text
 *   Text to find in
 * @param $regex
 *   Regular expression to find
 * @param $replacement_pattern
 *   Pattern to replace each match with
 * @param $max_replacements
 *   Limit replacements to the first $max_replacements matches.
 *   -1 = replace all matches
 * @param $settings
 * @param $state
 * @param $element
 * @return array
 *   An array of one element: the result_text variable containing a the
 *   result of performing a find and replace on $source_text
 * @throws \RulesEvaluationException
 *   An error occurred such as a problem parsing the regex
 */
function regex_transform($source_text, $regex, $replacement_pattern,
                         $max_replacements, $settings, $state, $element) {
  rules_log('regex_transform called with source = "%source", regex = "%regex", ' .
      'replace = "%replace", max replacments = "%max"',
    array(
      '%source' => $source_text,
      '%regex' => $regex,
      '%replace' => $replacement_pattern,
      '%max' => $max_replacements,
    ));
  $replace_result = preg_replace($regex, $replacement_pattern, $source_text, $max_replacements);

  if (is_null($replace_result)) {
    throw new RulesEvaluationException('Error %error in matching %source with %regex',
      array(
        '%error' => preg_last_error(),
        '%source' => $source_text,
        '%regex' => $regex,
      ),
      $element);
  }

  $result = array(
    'result_text' => $replace_result,
  );
  return $result;
}