--------------------------------------------------------------------------------
                               Rules Regex
--------------------------------------------------------------------------------

Maintainers:
 * Robert Black (blackra)

The Rules Regex module allows you to use a regular expression to transform the
data in a text parameter.

The module uses a match pattern and a replacement pattern. The input parameter
is not modified but the replaced result is output as a variable.

The module provides two variables as output: a list of matches based on the
regular expression patterns and subpatterns matched, and a text result based
on the replacement pattern.