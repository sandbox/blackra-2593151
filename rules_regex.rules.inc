<?php

/**
 * @file Includes any rules integration provided by the module.
 */


/**
 * Implements hook_rules_file_info().
 *
 * Specify files containing rules integration code.
 *
 * All files specified in that hook will be included when rules looks for
 * existing callbacks for any plugin. Rules remembers which callback is found in
 * which file and automatically includes the right file before it is executing
 * a plugin method callback. The file yourmodule.rules.inc is added by default
 * and need not be specified here.
 * This allows you to add new include files only containing functions serving as
 * plugin method callbacks in any file without having to care about file
 * inclusion.
 *
 * @return
 *   An array of file names without the file ending which defaults to '.inc'.
 */
function rules_regex_rules_file_info() {
  $items = array(
    'rules_regex.eval',
  );
  return $items;
}

/**
 * Implements hook_rules_category_info().
 *
 * Define categories for Rules items, e.g. actions, conditions or events.
 *
 * Categories are similar to the previously used 'group' key in e.g.
 * hook_rules_action_info(), but have a machine name and some more optional
 * keys like a weight, or an icon.
 *
 * For best compatibility, modules may keep using the 'group' key for referring
 * to categories. However, if a 'group' key and a 'category' is given the group
 * will be treated as grouping in the given category (e.g. group "paypal" in
 * category "commerce payment").
 *
 * @return
 *   An array of information about the module's provided categories.
 *   The array contains a sub-array for each category, with the category name as
 *   the key. Names may only contain lowercase alpha-numeric characters
 *   and underscores and should be prefixed with the providing module name.
 *   Possible attributes for each sub-array are:
 *   - label: The label of the category. Start capitalized. Required.
 *   - weight: (optional) A weight for sorting the category. Defaults to 0.
 *   - equals group: (optional) For BC, categories may be defined that equal
 *     a previsouly used 'group'.
 *   - icon: (optional) The file path of an icon to use, relative to the module
 *     or specified icon path. The icon should be a transparent SVG containing
 *     no colors (only #fff). See https://drupal.org/node/2090265 for
 *     instructions on how to create a suiting icon.
 *     Note that the icon is currently not used by Rules, however other UIs
 *     building upon Rules (like fluxkraft) do, and future releases of Rules
 *     might do as well. Consequently, the definition of an icon is optional.
 *     However, if both an icon font and icon is given, the icon is preferred.
 *   - icon path: (optional) The base path for the icon. Defaults to the
 *     providing module's directory.
 *   - icon font class: (optional) An icon font class referring to a suiting
 *     icon. Icon font class names should map to the ones as defined by Font
 *     Awesome, while themes might want to choose to provide another icon font.
 *     See http://fortawesome.github.io/Font-Awesome/cheatsheet/.
 *   - icon background color: (optional) The color used as icon background.
 *     Should have a high contrast to white. Defaults to #ddd.
 */

function rules_regex_rules_category_info() {
  $category_info = array(
    'regex' => array(
      'label' => t('Regex'),
      'equals group' => t('Regex'),
      'weight' => -10,
    ),
  );
  return $category_info;
}

/**
 * Implements hook_rules_action_info().
 *
 * Define rules compatible actions.
 *
 * This hook is required in order to add a new rules action. It should be
 * placed into the file MODULENAME.rules.inc, which gets automatically included
 * when the hook is invoked.
 *
 * However, as an alternative to implementing this hook, class based plugin
 * handlers may be provided by implementing RulesActionHandlerInterface. See
 * the interface for details.
 *
 * @return
 *   An array of information about the module's provided rules actions.
 *   The array contains a sub-array for each action, with the action name as
 *   the key. Actions names may only contain lowercase alpha-numeric characters
 *   and underscores and should be prefixed with the providing module name.
 *   Possible attributes for each sub-array are:
 *   - label: The label of the action. Start capitalized. Required.
 *   - group: A group for this element, used for grouping the actions in the
 *     interface. Should start with a capital letter and be translated.
 *     Required.
 *   - parameter: (optional) An array describing all parameter of the action
 *     with the parameter's name as key. Each parameter has to be
 *     described by a sub-array with possible attributes as described
 *     afterwards, whereas the name of a parameter needs to be a lowercase,
 *     valid PHP variable name.
 *   - provides: (optional) An array describing the variables the action
 *     provides to the evaluation state with the variable name as key. Each
 *     variable has to be described by a sub-array with possible attributes as
 *     described afterwards, whereas the name of a parameter needs to be a
 *     lowercase, valid PHP variable name.
 *   - 'named parameter': (optional) If set to TRUE, the arguments will be
 *     passed as a single array with the parameter names as keys. This emulates
 *     named parameters in PHP and is in particular useful if the number of
 *     parameters can vary. Defaults to FALSE.
 *   - base: (optional) The base for action implementation callbacks to use
 *     instead of the action's name. Defaults to the action name.
 *   - callbacks: (optional) An array which allows to set specific function
 *     callbacks for the action. The default for each callback is the actions
 *     base appended by '_' and the callback name.
 *   - 'access callback': (optional) A callback which has to return whether the
 *     currently logged in user is allowed to configure this action. See
 *     rules_node_integration_access() for an example callback.
 *  Each 'parameter' array may contain the following properties:
 *   - label: The label of the parameter. Start capitalized. Required.
 *   - type: The rules data type of the parameter, which is to be passed to the
 *     action. All types declared in hook_rules_data_info() may be specified, as
 *     well as an array of possible types. Also lists and lists of a given type
 *     can be specified by using the notating list<integer> as introduced by
 *     the entity metadata module, see hook_entity_property_info(). The special
 *     keyword '*' can be used when all types should be allowed. Required.
 *   - bundles: (optional) An array of bundle names. When the specified type is
 *     set to a single entity type, this may be used to restrict the allowed
 *     bundles.
 *   - description: (optional) If necessary, a further description of the
 *     parameter.
 *   - options list: (optional) A callback that returns an array of possible
 *     values for this parameter. The callback has to return an array as used
 *     by hook_options_list(). For an example implementation see
 *     rules_data_action_type_options().
 *   - save: (optional) If this is set to TRUE, the parameter will be saved by
 *     rules when the rules evaluation ends. This is only supported for savable
 *     data types. If the action returns FALSE, saving is skipped.
 *   - optional: (optional) May be set to TRUE, when the parameter isn't
 *     required.
 *   - 'default value': (optional) The value to pass to the action, in case the
 *     parameter is optional and there is no specified value.
 *   - 'allow null': (optional) Usually Rules will not pass any NULL values as
 *     argument, but abort the evaluation if a NULL value is present. If set to
 *     TRUE, Rules will not abort and pass the NULL value through. Defaults to
 *     FALSE.
 *   - restriction: (optional) Restrict how the argument for this parameter may
 *     be provided. Supported values are 'selector' and 'input'.
 *   - default mode: (optional) Customize the default mode for providing the
 *     argument value for a parameter. Supported values are 'selector' and
 *     'input'. The default depends on the required data type.
 *   - sanitize: (optional) Allows parameters of type 'text' to demand an
 *     already sanitized argument. If enabled, any user specified value won't be
 *     sanitized itself, but replacements applied by input evaluators are as
 *     well as values retrieved from selected data sources.
 *   - translatable: (optional) If set to TRUE, the provided argument value
 *     of the parameter is translatable via i18n String translation. This is
 *     applicable for textual parameters only, i.e. parameters of type 'text',
 *     'token', 'list<text>' and 'list<token>'. Defaults to FALSE.
 *   - ui class: (optional) Allows overriding the UI class, which is used to
 *     generate the configuration UI of a parameter. Defaults to the UI class of
 *     the specified data type.
 *   - cleaning callback: (optional) A callback that input evaluators may use
 *     to clean inserted replacements; e.g. this is used by the token evaluator.
 *   - wrapped: (optional) Set this to TRUE in case the data should be passed
 *     wrapped. This only applies to wrapped data types, e.g. entities.
 *  Each 'provides' array may contain the following properties:
 *   - label: The label of the variable. Start capitalized. Required.
 *   - type: The rules data type of the variable. All types declared in
 *     hook_rules_data_info() may be specified. Types may be parametrized e.g.
 *     the types node<page> or list<integer> are valid.
 *   - save: (optional) If this is set to TRUE, the provided variable is saved
 *     by rules when the rules evaluation ends. Only possible for savable data
 *     types. Defaults to FALSE.
 *
 *  The module has to provide an implementation for each action, being a
 *  function named as specified in the 'base' key or for the execution callback.
 *  All other possible callbacks are optional.
 *  Supported action callbacks by rules are defined and documented in the
 *  RulesPluginImplInterface. However any module may extend the action plugin
 *  based upon a defined interface using hook_rules_plugin_info(). All methods
 *  defined in those interfaces can be overridden by the action implementation.
 *  The callback implementations for those interfaces may reside in any file
 *  specified in hook_rules_file_info().
 *
 *  @see hook_rules_file_info()
 *  @see rules_action_execution_callback()
 *  @see hook_rules_plugin_info()
 *  @see RulesPluginImplInterface
 */
function rules_regex_rules_action_info() {
  $maximum_replacements_parameter = array(
    'label' => t('Maximum replacements'),
    'type' => 'integer',
    'description' => t('The maximum number of replacements that will be ' .
      'performed. Defaults to unlimited.'
    ),
    'default value' => -1,
    'optional' => TRUE,
    'restricted' => 'input',
  );
  $regex_parameter = array(
    'label' => t('Regular expression'),
    'type' => 'text',
    'description' => t('This regular expression supports the "imsSU" ' .
      'pattern modifiers of PCRE. Other modifiers will generate an error.'
    ),
    'default mode' => 'input',
  );
  $replacement_pattern_parameter = array(
    'label' => t('Replacement pattern'),
    'type' => 'text',
    'optional' => TRUE,
  );


  $action_info = array(
    'regex_match' => array(
      'label' => t('Find regex match'),
      'description' => t('Finds a regular expression in a text value.'),
      'group' => t('Regex'),
      'parameter' => array(
        'source_text' => array(
          'label' => t('Source text to process'),
          'type' => 'text',
          'description' => t('The source text is not modified by this match.'),
          'default mode' => 'selector',
          // If we don't set this to sanitize then HTML tags get stripped.
          'sanitize' => TRUE,
        ),
        'regex' => $regex_parameter,
      ),
      'provides' => array(
        'is_match' => array(
          'label' => t('Is a match?'),
          'type' => 'boolean',
        ),
        'matches' => array(
          'label' => t('List of matches'),
          'type' => 'list<text>',
          'description' => t('The first match in the list ' .
            'is the main pattern match, and subsequent list items are the ' .
            'matches to the sub-patterns.'
          ),
        ),
      ),
    ),
    'regex_replace' => array(
      'label' => t('Replace text field with regex substitution'),
      'group' => t('Regex'),
      'description' => t('Find and replace text in a text field using ' .
        'a regular expression.'
      ),
      'parameter' => array(
        'text_field' => array(
          'label' => t('Text field to modify'),
          'type' => 'text',
          'restriction' => 'selector',
          // If we don't set this to sanitize then HTML tags get stripped.
          'sanitize' => TRUE,
          'save' => TRUE,
        ),
        'regex' => $regex_parameter,
        'replacement_pattern' => $replacement_pattern_parameter,
        'maximum_replacements' => $maximum_replacements_parameter,
      ),
    ),
    'regex_transform' => array(
      'label' => t('Produce text value from regex substitution'),
      'group' => t('Regex'),
      'description' => t('Find and replace text using a regular expression. ' .
        'Do not overwrite original text field, but provide a variable ' .
        'containing the result instead.'
      ),
      'parameter' => array(
        'source_text' => array(
          'label' => t('Source text to process'),
          'type' => 'text',
          'restriction' => 'seclector',
          // If we don't set this to sanitize then HTML tags get stripped.
          'sanitize' => TRUE,
        ),
        'regex' => $regex_parameter,
        'replacement_pattern' => $replacement_pattern_parameter,
        'maximum_replacements' => $maximum_replacements_parameter,
      ),
      'provides' => array(
        'result_text' => array(
          'label' => t('Result text'),
          'type' => 'text',
        ),
      ),
    ),
  );
  return $action_info;
}
